console.log("EXT: Running Jira Extras Chrome Extension");
let running = false;
let lastTrigger = 0;

const config = { attributes: true, childList: true, subtree: true };

const callback = function (mutationsList, observer) {
  // Use traditional 'for loops' for IE 11
  for (const mutation of mutationsList) {
    if (mutation.target.localName == "tr" && mutation.addedNodes.length > 0) {
      if (lastTrigger < new Date().getTime() / 1000 - 1) {
        lastTrigger = new Date().getTime() / 1000;
        if (running == false) {
          running = true;
          RunUpdate();
        }
      }
    }
  }
};

// Create an observer instance linked to the callback function
const observer = new MutationObserver(callback);

function GetProjectTable() {
  return $("th")
    .filter(function () {
      return $(this).text() == "Key";
    })
    .closest("table");
}

const sleepNow = (delay) =>
  new Promise((resolve) => setTimeout(resolve, delay));

async function RunUpdate() {
  for (let i = 1; i <= 30; i++) {
    if (lastTrigger+0.5 > new Date().getTime() / 1000) {
      console.info("Skip this time.");
    } else {
      console.log("Run this time");
      AddEmptyIssuesColumn();
      ClearIssueColumn();
      AddTotals();
    
      running = false;
      break;
    }
    await sleepNow(250);
  }
}

$(function () {
  console.info("Trigger Page Loaded");
  let tableToWatch = GetProjectTable();
  AddEmptyIssuesColumn();
  observer.observe(tableToWatch[0], config);
  AddTotals();
});

function GetColumnNumberByHeadingText(headingText) {
  return $("th")
    .filter(function () {
      return $(this).text() == headingText;
    })
    .index();
}

function AddEmptyIssuesColumn() {
  let issueColNumber = GetColumnNumberByHeadingText("Issues");
  if (issueColNumber > 0) {
    return;
  }
  let projectTable = GetProjectTable();
  let keyColNumber = GetColumnNumberByHeadingText("Key");
  console.warn();(`keyColNumber = ${keyColNumber}`);
  projectTable.find("tr").each(function () {
    $(this).find("th").eq(keyColNumber).after("<th>Issues</th>");
    $(this).find("td").eq(keyColNumber).after(`<td>${chrome.i18n.getMessage("loading")}</td>`);
  });
}

function ClearIssueColumn() {
  let issueColNumber = GetColumnNumberByHeadingText("Issues");
  if (issueColNumber < 1) {
    return;
  }
  let projectTable = GetProjectTable();
  projectTable.find("tr").each(function () {
    $(this).find("td").eq(issueColNumber).text(chrome.i18n.getMessage("loading"));
  });
}

function AddTotals() {
  $.getJSON("/rest/api/3/project", function (json) {
    let projectArr = [];
    json.forEach(function (item) {
      console.log("name: " + item.name);
      console.log("id: " + item.id);
      console.log("key: " + item.key);
      projectArr.push(item.key);
    });
    GetProjectTotals(projectArr);
  });
}

function GetProjectTotals(projects) {
  const promises = [];
  projects.forEach(function (item) {
    console.log("GetTotal for project: " + item);
    promises.push(
      $.ajax({
        url: `/rest/api/3/search?jql=project = "${item}"`,
        tryCount: 0,
        retryLimit: 5,
        contentType: "application/json",
        dataType: "json",
        success: function(json) {
          console.log(`Project ${item}: has ${json.total} issues`);
          UpdateIssueColumnWithValue(item, json.total);  
        },
        error: function(response, status, error) {
          if (response.status == 429) {
            this.tryCount++;
            if (this.tryCount <= this.retryLimit) {
              console.log('failure');
              setTimeout(() => {
                console.log('retrying');
                $.ajax(this);
              }, 3000);
              return;
            } else {
              console.log('update failed!');
            }
          }
        }
      })
    );
  });

  Promise.all(promises)
    .then(() => {
      let tableToWatch = GetProjectTable();
    })
    .catch((e) => {
      console.info(e);
    });
}

async function UpdateIssueColumnWithValue(projectKey, issueCount) {
  let issueColNumber = GetColumnNumberByHeadingText("Issues");
  let tableRow = $("td")
    .filter(function () {
      return $(this).text() == projectKey;
    })
    .closest("tr");
  tableRow.find("td").eq(issueColNumber).text(issueCount);
  console.log(`EXT: ${projectKey} set to ${issueCount}`);
}
